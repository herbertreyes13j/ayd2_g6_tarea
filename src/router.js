const express = require("express")
const router = express.Router()
const controller = require("./controllers/controller")

router.get("/", controller.renderHomePage)
router.post("/", controller.insertarusuarior)
router.get("/about", controller.renderAboutPage)
router.get("/ver",controller.renderVerPage)
module.exports = router