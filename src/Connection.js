'use strict';
const mysql = require('mysql')
//local mysql db connection
const dbConn = mysql.createConnection({
  host     : '34.66.87.156',
  port     : '32662',
  user     : 'analisis',
  password : 'secret',
  database : 'analisis'
});

dbConn.connect(function(err) {
  if (err) throw err;
  console.log("Database Connected!");
});

module.exports = dbConn;