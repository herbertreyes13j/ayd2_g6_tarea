var assert = require('assert');

var controller = require('./../src/controllers/controller.js');


describe('Prueba a controllers', () => {

    it('Página principal', function(){
        assert.equal(controller.renderHomePage(), true);
    });

    it('Ver', function(){
        assert.equal(controller.renderVerPage(), true);
    });

    it('insertarusuarior() erroneo, sin datos', function(){
        assert.equal(controller.insertarusuarior(), false);
    });

    let p1 = {
        body: {
            nombre:'nombre',
            apellido:'apellido',
            email:'email',
            phone:'phone',
            address:'address'
        }
    }

    it('insertarusuarior() valido, con datos', function(){
        assert.equal(controller.insertarusuarior(p1), true);
    });

    it('About', function(){
        assert.equal(controller.renderAboutPage(), true);
        process.exit();
    });


});
        