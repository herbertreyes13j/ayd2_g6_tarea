CREATE DATABASE IF NOT EXISTS analisis;
USE analisis;
CREATE USER 'analisis'@'localhost' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON * . * TO 'analisis'@'localhost';
FLUSH PRIVILEGES;
CREATE TABLE IF NOT EXISTS usuario(
    cod_usuario INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255),
    apellido VARCHAR(255),
    correo VARCHAR(255),
	tel_usuario INT,
    direccion VARCHAR(255)
);